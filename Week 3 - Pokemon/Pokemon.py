from enum import Enum
import random

############################################################ CLASSES:

class Direction( Enum ):
	UP = 0
	DOWN = 1
	LEFT = 2
	RIGHT = 3

class Space( Enum ):
	POKEMON = 0
	WALL = 1
	NOTHING = 2
	PLAYER = 3

class Player:
	def __init__( self ):
		self.position = ( 0, 0 )

class Pokemon:
	def __init__( self, name, types, level ):
		self.name = name
		self.types = types
		self.level = level

class PokemonWorld:
	def __init__( self, width, height ):
		self.player_space = ( -1, -1 )
		self.is_game_over = False
		self.width = width
		self.height = height
		self.spaces = []
		self.spaces_explored_statuses = []
		for i in range( 0, width ):
			self.spaces.append( [] )
			self.spaces_explored_statuses.append( [] )
			for j in range( 0, height ):
				self.spaces[i].append( self.gen_random_space() )
				self.spaces_explored_statuses[i].append( False )

		self.spaces[0][0] = Space.NOTHING
		self.spaces[1][0] = Space.NOTHING
		self.spaces[0][1] = Space.NOTHING
		self.spaces[1][1] = Space.NOTHING

	def gen_random_space( self ):
		rand = random.randint( 0, 99 )
		if rand < 8:
			return Space.POKEMON
		elif rand < 35:
			return Space.WALL
		else:
			return Space.NOTHING

	def is_in_bounds( self, x, y ):
		return ( x >= 0 and x < len( self.spaces ) ) and \
				( y >= 0 and y < len( self.spaces[0] ) )
	
	def illuminate_surrounding_squares( self, x, y ):
		self.player_space = ( x, y )
		for i in range( x - 1, x + 2 ):
			for j in range( y - 1, y + 2 ):
				if self.is_in_bounds( i, j ):
					self.spaces_explored_statuses[i][j] = True

class PokemonGame:

	def add_dir_to_pos( self, pos, direction ):
		x = pos[0]
		y = pos[1]
		if direction == Direction.UP:
			y += 1
		elif direction == Direction.LEFT:
			x -= 1
		elif direction == Direction.RIGHT:
			x += 1
		elif direction == Direction.DOWN:
			y -= 1
		return ( x, y )


	def can_move( self, direction ):
		( x, y ) = self.add_dir_to_pos( self.player.position, direction )
		is_wall = self.world.spaces[x][y] == Space.WALL
		return self.world.is_in_bounds( x, y ) and not is_wall

	def move_player_in_dir( self, direction ):
		( x, y ) = self.add_dir_to_pos( self.player.position, direction )
		self.move_player_to( x, y )

	def move_player_to( self, x, y ):
		# Reveal surrounding squares
		# Fight a pokemon if necessary
			# Do combat
			# Determine if win 

		self.world.illuminate_surrounding_squares( x, y )
		self.player.position = ( x, y )

	def new_game( self, x, y ):
		self.world = PokemonWorld( 10, 10 )
		self.player = Player()
		self.move_player_to( 0, 0 )

########################################################## FUNCTIONS:

def get_move():
	move_dir = input( "Where you moving? (use wasd) " )
	possible_dirs = ['w', 'a', 's', 'd']
	while len( move_dir ) == 0 or not move_dir in possible_dirs:
		print( "This isn't complicated...press w for up, a for left, " + \
			   "d for right, or s for down" )
		move_dir = input( "Where you moving? " )
	if move_dir == 'w':
		return Direction.UP
	elif move_dir == 'a':
		return Direction.LEFT
	elif move_dir == 'd':
		return Direction.RIGHT
	else: # move_dir == 's'
		return Direction.DOWN

def print_world( game ):
	printout = []
	world = game.world
	for y in range( world.height - 1, -1, -1 ):
		for x in range( world.width ):
			if ( x, y ) == game.player.position:
				printout.append( "@" )
			elif not world.spaces_explored_statuses[x][y]:
				printout.append( "?" )
			elif world.spaces[x][y] == Space.POKEMON:
				printout.append( "P" )
			elif world.spaces[x][y] == Space.WALL:
				printout.append( "#" )
			elif world.spaces[x][y] == Space.NOTHING:
				printout.append( "." )
		printout.append( "\n" )
	print( "".join( printout ) )

############################################################### MAIN:

def play():
	game = PokemonGame()
	game.new_game( 10, 10 )
	print_world( game )

	while True:
		move_dir = get_move()
		if game.can_move( move_dir ):
			game.move_player_in_dir( move_dir )
			print_world( game )
		else:
			print( "Can NOT move", move_dir )

	# game = PokemonGame()
	# game.start_new_game( 10, 10 ) 
	# game.print()


play()