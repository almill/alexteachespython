import random
import string

# random_letter = random.choice( letters )
# random_number = random.choice( digits )
# print( random_letter )
# print( random_number )

def gen_plates( num ):
	letters = string.ascii_uppercase
	digits = string.digits
	plates = set()
	while len( plates ) < num:
		new_plate = random.choice( letters ) + \
					random.choice( letters ) + \
					random.choice( digits )  + \
					random.choice( digits )  + \
					random.choice( digits )
		plates.add( new_plate )
	return list( plates )

def create_database_from_list( list ):
	return list.copy()

def make_simple_corrections( license ):
	license = license.trim()
