import random
import sys

# This line is a comment, it doesn't make the program do anything.
# The program knows it's a comment becuase it starts with a #

def ask_how_much_to_feed( grain ):
	"Ask user how much grain to feed people"
	to_feed = get_int( "O Cool Hammurabi, how much grain will you feed to your people?  " )
	while to_feed > grain:
		print( "We've only got ", grain, " units of grain" )
		to_feed = get_int( "How much grain will you feed to your people?  " )
	return to_feed

def ask_how_many_to_plant( bushels_in_storage, acres_owned, population ):
	"Ask user how many acres to plant"
	while True:
		acres_to_plant = get_int( "O Swell Hammurabi, how many acres will you plant?  " )
		if acres_to_plant > acres_owned:
			print( "We've only got ", acres_owned, " acres" )
			continue

		if 2 * acres_to_plant > bushels_in_storage:
			print( "We've only got ", bushels_in_storage, " bushels of grain" )
			continue

		if acres_to_plant > population * 10:
			print( "We've only got ", population, " farmers" )
			continue
		break
	return acres_to_plant

def ask_to_buy_land( bushels, cost ):
	"Ask user how many bushels to spend buying land."
	# We call our get_int() function.  It prints the given message
	# and then returns the integer the user enters
	acres = get_int( "How many acres will you buy? " )
	# If the user wants to buy more than they can afford, we will
	# ask them over and over to give a good number in this while
	# loop
	while acres * cost > bushels:
		print( "O great Hammurabi, we have but", bushels, "bushels of grain!" )
		# Get a new integer from user, and assign it to acres. If
		# the while condition is satisfied, we'll loop through again
		acres = get_int( "How much land will you buy? " )
	return acres

def ask_to_sell_land( acres_owned ):
	"Ask user how much land to sell."
	acres = get_int( "How many acres will you sell? " )
	while acres > acres_owned:
		print( "O, wise and mathematically-inclined Hammurabi, we only  have ", acres_owned, " acres" )
	return acres

def get_bushels_eaten_by_rats( bushels_in_storage ):
	if random.randint( 0, 60 ):
		return 0
	return bushels_in_storage * float( random.randint( 1, 3 ) ) / 10

def get_harvest( acres ):	
	return acres * random.randint( 1, 8 )

def get_int( message ):
	answer = None
	while answer == None:
		try:
			answer = int( input( message ) )
		except KeyboardInterrupt:
			print( "Goodbye\n" )
			sys.exit( 0 )
		except:
			print( "Please enter a number" )
	return answer

def get_immigrants( acres, bushels_in_storage, population ):
	return int( ( 20 * acres + bushels_in_storage ) / ( 100 * population ) ) + 1

def get_new_cost_of_land():
	return random.randint( 17, 23 )

def get_plague_deaths( population ):
	if random.randint( 0, 99 ) < 15:
		return math.ceil( population / 2 )
	return 0

def get_starve_deaths( population, bushels_in_storage ):
	"Returns new population, new bushels, and too_many_starved"
	bushels_needed = population * 20
	if bushels_needed <= bushels_in_storage:
		return 0
	shortage = bushels_needed - bushels_in_storage
	return math.ceil( shortage / 20.0 )

def print_introductory_message():
	message = "Congratulations, you are the newest ruler of ancient Samaria, elected" 	+ "\n" + \
			  "for a ten year term of office. Your duties are to dispense food, direct" + "\n" + \
			  "farming, and buy and sell land as needed to support your people. Watch" 	+ "\n" + \
			  "out for rat infestations and the plague! Grain is the general currency," + "\n" + \
			  "measured in bushels. The following will help you in your decisions:" 	+ "\n" + \
			  "" 																		+ "\n" + \
			  "  * Each person needs at least 20 bushels of grain per year to survive." + "\n" + \
			  "" 																		+ "\n" + \
			  "  * Each person can farm at most 10 acres of land." 						+ "\n" + \
			  "" 																		+ "\n" + \
			  "  * It takes 2 bushels of grain to farm an acre of land." 				+ "\n" + \
			  "" 																		+ "\n" + \
			  "  * The market price for land fluctuates yearly." 						+ "\n" + \
			  "" 																		+ "\n" + \
			  "Rule wisely and you will be showered with appreciation at the end of" 	+ "\n" + \
			  "your term. Rule poorly and you will be kicked out of office!"
	print( "\n\n" )
	print( message )

def hammurabi():
	
	print_introductory_message()

	starved = 0
	immigrants = 5
	population = 100
	harvest = 3000          # total bushels harvested
	bushels_per_acre = 3    # amount harvested for each acre planted
	rats_ate = 200          # bushels destroyed by rats
	bushels_in_storage = 2800
	acres_owned = 1000
	cost_per_acre = 19      # each acre costs this many bushels
	plague_deaths = 0

	for year in range( 1, 11 ):
		print( "\n********************************* YEAR", year, "*********************************\n" )
		print( "O great Hammurabi!" )
		print( "You are in year ", year, " of your ten year rule." )
		print( "In the previous year ", starved, " people starved to death." )
		print( "In the previous year ", immigrants, " people entered the kingdom." )
		print( "The population is now", population, "." )
		print( "We harvested ", harvest, " bushels at", cost_per_acre, "bushels per acre." )
		print( "Rats destroyed ", rats_ate, " bushels, leaving ", bushels_in_storage, " bushels in storage." )
		print( "The city owns ", acres_owned, " acres of land." )
		print( "Land is currently worth ", cost_per_acre, " bushels per acre." )
		print( "There were ", plague_deaths, " deaths from the plague." )
		print( "\n" )

		# Figure out how much land we'll buy or sell
		to_buy = ask_to_buy_land( bushels_in_storage, cost_per_acre )
		to_sell = 0
		if to_buy == 0:
			to_sell = ask_to_sell_land( acres_owned )

		# Adjust our bushels_in_storage before we decide how much to
		# feed so we don't double-spend our bushels
		bushels_in_storage -= to_buy * cost_per_acre
		bushels_in_storage += to_sell * cost_per_acre
		acres_owned += to_buy
		acres_owned -= to_sell

		# Ask the user for the rest of their input
		bushels_to_feed = ask_how_much_to_feed( bushels_in_storage )
		acres_to_plant = ask_how_many_to_plant( bushels_in_storage, acres_owned, population )

		# Time for our "simulation"
		dead_from_plague = get_plague_deaths( population )
		population -= dead_from_plague
		starved = get_starve_deaths( population, bushels_in_storage )
		population -= starved
		if starved == 0:
			arrived = get_immigrants( acres_owned, bushels_in_storage, population )
			population += arrived
		harvest = get_harvest( acres_owned )
		bushels_in_storage += harvest
		eaten_by_rats = get_bushels_eaten_by_rats( bushels_in_storage )
		bushels_in_storage -= eaten_by_rats
		cost_per_acre = get_new_cost_of_land()

hammurabi()
